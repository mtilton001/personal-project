import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.List;

/**
 *
 * Java Get_Employee
 *
 * Get_Employee: class queries employee table for all current employees
 *
 ***********************************************
 *********    Included Requirements    *********
 ***********************************************
 *        Java Collections - List Array        *
 *    Exception Handling & Data Validation     *
 *                Hibernate                    *
 ***********************************************
 *
 * @author Matthew Tilton
 *
 */

public class Get_Employee {
    public static void main(String[] args) {
        try {
            //create or initialize ManagerFactory using persistence.xml unit name
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
            EntityManager entityManager = entityManagerFactory.createEntityManager();

            List<Object[]> resultList = entityManager.createNativeQuery(
                    "SELECT n.Last_Name, n.First_Name, n.Middle_Initial, n.Preferred_Name, e.PID, n.Name_ID, p.Address_ID\n" +
                            "FROM employee_db.employee e\n" +
                            "join employee_db.name n on n.Name_ID = e.Name_ID\n" +
                            "join employee_db.physical_address p on p.Address_ID = e.Address_ID;")
                    .getResultList();

            System.out.format("|%15s |%15s |%15s |%15s |%15s |\n", "Last Name", "First Name", "Middle Initial", "Preferred Name", "PID");
            System.out.format("_______________________________________________________________________________________\n");
            for (Object[] result : resultList) {

                System.out.format("|%15s |%15s |%15s |%15s |%15s |\n", result[0], result[1], result[2], result[3], result[4]);
            }

            entityManagerFactory.close();

            System.out.println("\n");
            Main.main(null);
        }
        catch (Exception e){
            System.out.println("The system has failed to provide the Employee Table.");
        }
    }
}