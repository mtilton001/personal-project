import com.smartystreets.api.ClientBuilder;
import com.smartystreets.api.StaticCredentials;
import com.smartystreets.api.exceptions.SmartyException;
import com.smartystreets.api.us_zipcode.City;
import com.smartystreets.api.us_zipcode.Client;
import com.smartystreets.api.us_zipcode.Lookup;
import com.smartystreets.api.us_zipcode.Result;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.List;

/**
 *
 * Servlet: Used to serve response to either html post or get
 *
 * Creates new employee entry in the Name, Physical_Address and Employee tables
 *
 ***********************************************
 *********    Included Requirements    *********
 ***********************************************
 *        Java Collections - List Array        *
 *    Exception Handling & Data Validation     *
 *                  JSON                       *
 *                Hibernate                    *
 *                 Servlet                     *
 ***********************************************
 *
 * @author Matthew Tilton
 *
 */

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    //post method
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            //String variables set with contents entered on index.jsp HTML form
            String lastName = request.getParameter("lastName");
            String firstName = request.getParameter("firstName");
            String middleInitial = request.getParameter("middleInitial");
            String preferredName = request.getParameter("preferredName");
            String dob = request.getParameter("dob");
            String ssn = request.getParameter("ssn");
            String street = request.getParameter("street");
            String postalCode = request.getParameter("postalCode");
            String city;
            String state;
            String phone = request.getParameter("phoneNumber");
            String email = request.getParameter("email");
            String department = request.getParameter("department");
            String jobTitle = request.getParameter("jobTitle");
            String officePhone = request.getParameter("officePhone");
            String officeNumber = request.getParameter("officeNumber");
            String hireDate = request.getParameter("hireDate");
            String trainingCompleteDate = request.getParameter("trainingCompleteDate");

            System.out.println(firstName + " | " +  middleInitial + " | " + lastName + " | " + preferredName + " | \n"
                    + dob + " | " + ssn + " | \n"
                    + street + " | " + postalCode + " | \n"
                    + phone + " | " + email + " | \n"
                    + department + " | " + jobTitle + " | " + officePhone + " | " + officeNumber + " | \n"
                    + hireDate +  " | " + trainingCompleteDate);

            /*
             *
             * Name Table:
             *
             */
            while (lastName.length() > 45) {
                lastName = request.getParameter("lastName");
            }

            while (firstName.length() > 45) {
                firstName = request.getParameter("firstName");
            }

            while (middleInitial.length() > 1) {
                middleInitial = request.getParameter("middleInitial");
            }

            while (preferredName.length() > 45) {
                preferredName = request.getParameter("preferredName");
            }

            /*
             *
             * Address Table:
             *
             * Zip to City & State: Start
             *
             */
            while (street.length() > 45) {
                street = request.getParameter("street");
            }

            while (postalCode.length() > 5) {
                postalCode = request.getParameter("postalCode");
            }

            /*
             * Secret keys usually stored in environment variables.
             * Authentication for API requests: Using smartystreets.com
             */
            String authId = "1c573638-e827-321f-bd5f-34785335d15f";
            String authToken = "F4KM9U3KZJS76LRov8ST";
            StaticCredentials credentials = new StaticCredentials(authId, authToken);

            Client client = new ClientBuilder(credentials).buildUsZipCodeApiClient();
            Lookup lookup = new Lookup();
            lookup.setZipCode(postalCode);

            try {
                client.send(lookup);
            }
            catch (SmartyException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            Result zipResult = lookup.getResult();
            City cities = zipResult.getCity(0);

            city = cities.getCity();
            state = cities.getState();
            /*
             * Zip to City & State: End
             *
             *
             * Static entry for country
             * Could be set like postal code with further API use
             */
            String country = "United States";

            /*
             *
             * Employee Table:
             *
             */

            while (!Validate.checkDate(dob)) {
                dob = request.getParameter("dob");
            }

            while (!Validate.checkSSN(ssn)) {
                ssn = request.getParameter("ssn");
            }

            while (!Validate.checkPhone(phone)) {
                phone = request.getParameter("phone");
            }

            while (!Validate.checkEmail(email)) {
                email = request.getParameter("email");
            }

            while (jobTitle.length() > 45) {
                jobTitle = request.getParameter("jobTitle");
            }

            while (department.length() > 45) {
                department = request.getParameter("department");
            }

            while (!Validate.checkPhone(officePhone)) {
                officePhone = request.getParameter("officePhone");
            }

            while (officeNumber.length() > 15) {
                officeNumber = request.getParameter("officeNumber");
            }

            while (!Validate.checkDate(hireDate)) {
                hireDate = request.getParameter("hireDate");
            }

            while (!Validate.checkDate(trainingCompleteDate)) {
                trainingCompleteDate = request.getParameter("trainingCompeteDate");
            }

            //create or initialize ManagerFactory using persistence.xml unit name
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
            EntityManager entityManager = entityManagerFactory.createEntityManager();

            int newNameID = 0;
            int newAddressID = 0;

            List<Object[]> resultList = entityManager.createNativeQuery(
                    "SELECT n.Name_ID, p.Address_ID\n" +
                            "FROM employee_db.employee e\n" +
                            "join employee_db.name n on (n.Name_ID)\n" +
                            "join employee_db.physical_address p on (p.Address_ID)\n" +
                            "ORDER BY n.Name_ID DESC, p.Address_ID DESC\n" +
                            "LIMIT 1;")
                    .getResultList();

            for (Object[] result : resultList) {
                int NameID = (int) result[0];
                int AddressID = (int) result[1];

                newNameID = NameID + 1;
                newAddressID = AddressID + 1;
            }
            //System.out.println(newNameID + " " + newAddressID);

            if (newNameID > 0 || newAddressID > 0) {
                Name name = new Name(lastName, firstName, middleInitial, preferredName);
                Physical_Address physicalAddress = new Physical_Address(street, city, state, postalCode, country);
                Employee employee = new Employee(newNameID, dob, ssn, phone, newAddressID, email, department, jobTitle, officePhone, officeNumber, hireDate, trainingCompleteDate);

                entityManager.getTransaction().begin();
                entityManager.persist(name);
                entityManager.getTransaction().commit();

                entityManager.getTransaction().begin();
                entityManager.persist(physicalAddress);
                entityManager.getTransaction().commit();

                entityManager.getTransaction().begin();
                entityManager.persist(employee);
                entityManager.getTransaction().commit();
            }
            else {
                System.out.println("Id's not greater than 0: " + newNameID + " | " + newAddressID);
            }

            entityManagerFactory.close();

        //Setting response content and encoding
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        //PrintWriter setup with html response data
        PrintWriter write = response.getWriter();
        write.append("      <!DOCTYPE html>")
                .append("   <html>")
                .append("       <head>")
                .append("       <meta http-equiv=\"refresh\" content=\"7;url=http://localhost:8080/CIT_360_Personal_Project_war_exploded/\" />")
                .append("           <title>Servlet</title>")
                .append("       </head>")
                .append("       <body>")
                .append("           <h2>Employee Successfully Submitted</h2>")
                .append("           <p>" + firstName +  " | " + middleInitial +  " | " + lastName + " | " + preferredName + "</p>")
                .append("           <p>" + dob + " | " + ssn + "</p>")
                .append("           <p>" + street +  " | " + city +  " | " + state +  " | " + country + "</p>")
                .append("           <p>" + phone +  " | " +  email + "</p>")
                .append("           <p>" + department +  " | " + jobTitle  + " | " + officePhone +  " | " + officeNumber + "</p>")
                .append("           <p>" + hireDate +  " | " + trainingCompleteDate + "</p>")
                .append("           <h3>Redirecting in 7 seconds</h3>")
                .append("       </body>")
                .append("   </html>");

        }
        catch (Exception e) {
            //Setting response content and encoding
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");

            //PrintWriter setup with html response data
            PrintWriter write = response.getWriter();
            write.append("      <!DOCTYPE html>")
                    .append("   <html>")
                    .append("       <head>")
                    .append("           <title>Servlet</title>")
                    .append("       </head>")
                    .append("       <body>")
                    .append("           <h2>Employee Submission Failed: Please try again!</h2>")
                    .append("       </body>")
                    .append("   </html>");

            System.out.println("The system has failed to add an employee.");
        }
    }

    //get method
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //Setting response content and encoding
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        //PrintWriter setup with html response data
        PrintWriter write = response.getWriter();
        write.append("      <!DOCTYPE html>")
                .append("   <html>")
                .append("       <head>")
                .append("           <title>Servlet</title>")
                .append("       </head>")
                .append("       <body>")
                .append("           <h2>This is my http get within my java Servlet</h2>")
                .append("       </body>")
                .append("   </html>");
    }
}