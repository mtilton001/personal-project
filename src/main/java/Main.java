import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * Java Main
 *
 * Main class calls either Add_Employee or Get_Employee class
 *
 ***********************************************
 *********    Included Requirements    *********
 ***********************************************
 *    Exception Handling & Data Validation     *
 ***********************************************
 *
 * @author Matthew Tilton
 *
 */

public class Main {
    public static void main(String[] args) {
        boolean notInt = false;
        int select = 0;

        do {
            try {
                Scanner input = new Scanner(System.in);

                System.out.println("Please make selection:\n" +
                        "1) Add New Employee\n" +
                        "2) Modify Employee\n" +
                        "3) Get Employee List\n" +
                        "4) End Program\n");

                select = input.nextInt();
                while (select <= 0 || select > 4) {
                    System.out.println("Please make a selection (1 - 4):");
                    select = input.nextInt();
                }

                switch (select) {
                    case 1 -> Add_Employee.main(null);
                    case 2 -> Modify_Employee.main(null);
                    case 3 -> Get_Employee.main(null);
                    case 4 -> {
                        System.out.println("Goodbye!");
                        System.exit(0);
                    }
                    default -> System.out.println("No selection made:");
                }
            }
            catch (InputMismatchException e) {
                System.out.println("Invalid Entry:");
                notInt = true;
            }
        }
        while (notInt);
    }
}