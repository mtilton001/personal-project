import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;

/**
 *
 * Java Name
 *
 * Name class creating database table mappings
 *
 ***********************************************
 *********    Included Requirements    *********
 ***********************************************
 *          Persistence for Hibernate          *
 ***********************************************
 *
 * @author Matthew Tilton
 *
 */

@Entity
//assigns the table(s) to look at
@Table(name = "name")

public class Name implements Serializable {

    //designate primary key with @Id
    @Id
    @Column(name = "Name_ID")
    private int nameID;

    @Column(name = "Last_Name")
    private String lastName;

    @Column(name = "First_Name")
    private String firstName;

    @Column(name = "Middle_Initial")
    private String middleInitial;

    @Column(name = "Preferred_Name")
    private String preferredName;

    //Getters & Setters
    public int getNameID() {
        return nameID;
    }

    public void setNameID(int nameID) {
        this.nameID = nameID;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public String getPreferredName() {
        return preferredName;
    }

    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    //Constructors
    public Name() {
    }

    public Name(String lastName, String firstName, String middleInitial, String preferredName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.preferredName = preferredName;
    }
}
