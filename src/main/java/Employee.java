import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;

/**
 *
 * Java Employee
 *
 * Employee class creating database table mappings
 *
 ***********************************************
 *********    Included Requirements    *********
 ***********************************************
 *          Persistence for Hibernate          *
 ***********************************************
 *
 * @author Matthew Tilton
 *
 */

@Entity
//assigns the table(s) to look at
@Table(name = "employee")

public class Employee implements Serializable {

    //designate primary key with @Id
    @Id
    //map pid column from table to pid variable with @Column
    @Column(name = "pid")
    private int pid;

    @Column(name = "Name_ID")
    private int nameID;

    @Column(name = "DOB")
    private String dob;

    @Column(name = "SSN")
    private String ssn;

    @Column(name = "Phone")
    private String phone;

    @Column(name = "Address_ID")
    private int addressID;

    @Column(name = "Email")
    private String email;

    @Column(name = "Department")
    private String department;

    @Column(name = "Job_Title")
    private String jobTitle;

    @Column(name = "Office_Phone")
    private String officePhone;

    @Column(name = "Office_Number")
    private String officeNumber;

    @Column(name = "Hire_Date")
    private String hireDate;

    @Column(name = "Training_Complete_Date")
    private String trainingCompleteDate;

    //Getters & Setters

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getNameID() {
        return nameID;
    }

    public void setNameID(int nameID) {
        this.nameID = nameID;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getHireDate() {
        return hireDate;
    }

    public void setHireDate(String hireDate) {
        this.hireDate = hireDate;
    }

    public String getTrainingCompleteDate() {
        return trainingCompleteDate;
    }

    public void setTrainingCompleteDate(String trainingCompleteDate) {
        this.trainingCompleteDate = trainingCompleteDate;
    }

    //Constructors
    public Employee() {
    }

    public Employee(int nameID, String dob, String ssn, String phone, int addressID, String email, String department, String jobTitle, String officePhone, String officeNumber, String hireDate, String trainingCompleteDate) {
        this.nameID = nameID;
        this.dob = dob;
        this.ssn = ssn;
        this.phone = phone;
        this.addressID = addressID;
        this.email = email;
        this.department = department;
        this.jobTitle = jobTitle;
        this.officePhone = officePhone;
        this.officeNumber = officeNumber;
        this.hireDate = hireDate;
        this.trainingCompleteDate = trainingCompleteDate;
    }
}
