import org.apache.commons.validator.routines.EmailValidator;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

/**
 *
 * Java Validate
 *
 * Validate class: the validator for client data entry
 *
 ***********************************************
 *********    Included Requirements    *********
 ***********************************************
 *    Exception Handling & Data Validation     *
 ***********************************************
 *
 * @author Matthew Tilton
 *
 */

public class Validate {
    public static boolean checkPhone(String phone) {
        try {
            /*
             * Create regex validator and return true or false
             */
            String regex = "^(\\d{3}[- .]?){2}\\d{4}$";

            return phone.matches(regex);
        }
        catch (Exception e) {
            System.out.println("checkPhone validation could not complete.");
            return false;
        }
    }

    public static boolean checkEmail(String email) {
        try {
            /*
             * Create validator and return true or false
             */
            EmailValidator emailValidator = EmailValidator.getInstance();

            return emailValidator.isValid(email);
        }
        catch (Exception e) {
            System.out.println("checkEmail validation could not complete.");
            return false;
        }
    }

    public static boolean checkDate(String stringDate) {
        try {
            /*
             * Check if date is 'null'
             */
            if (!stringDate.trim().equals("")) {
                /*
                 * Preferred date format,
                 */
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                simpleDateFormat.setLenient(false);

                /*
                 * Try parse string into date
                 */
                try {
                    Date javaDate = simpleDateFormat.parse(stringDate);
                    //System.out.println(stringDate + "True");
                }
                /*
                 * Catch when format is invalid
                 */ catch (ParseException e) {
                    //System.out.println(stringDate + "False");
                    return false;
                }
            }

            /*
             * Return ture when format is valid
             */
            return true;
        }
        catch (Exception e) {
            System.out.println("checkDate validation could not complete.");
            return false;
        }
    }

    public static boolean checkSSN(String ssn) {
        try {
            /*
             * Create regex validator and return true or false
             */
            String regex = "^(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}$";

            return ssn.matches(regex);
        }
        catch (Exception e) {
            System.out.println("checkSSN validation could not complete.");
            return false;
        }
    }
}