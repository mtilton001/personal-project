import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;

/**
 *
 * Java Physical_Address
 *
 * Physical_Address class creating database table mappings
 *
 ***********************************************
 *********    Included Requirements    *********
 ***********************************************
 *          Persistence for Hibernate          *
 ***********************************************
 *
 * @author Matthew Tilton
 *
 */

@Entity
//assigns the table(s) to look at
@Table(name = "physical_address")

public class Physical_Address implements Serializable {

    //designate primary key with @Id
    @Id
    @Column(name = "Address_ID")
    private int addressID;

    @Column(name = "Street")
    private String street;

    @Column(name = "City")
    private String city;

    @Column(name = "State")
    private String state;

    @Column(name = "Postal_Code")
    private String postalCode;

    @Column(name = "Country")
    private String country;

    //Getters and Setters

    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        this.addressID = addressID;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    //Constructors
    public Physical_Address() {
    }

    public Physical_Address(String street, String city, String state, String postalCode, String country) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
    }
}
