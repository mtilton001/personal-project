import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.io.IOException;

/* *** smartystreets.com source imports for API *** */
import com.smartystreets.api.ClientBuilder;
import com.smartystreets.api.StaticCredentials;
import com.smartystreets.api.exceptions.SmartyException;
import com.smartystreets.api.us_zipcode.Lookup;
import com.smartystreets.api.us_zipcode.City;
import com.smartystreets.api.us_zipcode.Client;
import com.smartystreets.api.us_zipcode.Result;
/* Dependencies added to pom.xml
 * Documentation for input fields can be found at:
 * https://smartystreets.com/docs/us-zipcode-api#input-fields
 * *** smartystreets.com API output is JSON ***
 */

/**
 *
 * Java Add_Employee
 *
 * Add_Employee class: creates new employee entry in the Name, Physical_Address and Employee tables
 *
 ***********************************************
 *********    Included Requirements    *********
 ***********************************************
 *        Java Collections - List Array        *
 *    Exception Handling & Data Validation     *
 *                  JSON                       *
 *                Hibernate                    *
 ***********************************************
 *
 * @author Matthew Tilton
 *
 */

public class Add_Employee {
    public static void main(String[] args) {
        try {
            Scanner entry = new Scanner(System.in);
            System.out.println("Please enter employee information\n");

            /*
             *
             * Name Table:
             *
             */
            System.out.println("Last Name:");
            String lastName = entry.nextLine();
            while (lastName.length() > 45) {
                System.out.println("45 characters or less | Please enter a valid Last Name:");
                lastName = entry.nextLine();
            }
            System.out.println("\n" + lastName + " |\n");

            System.out.println("First Name:");
            String firstName = entry.nextLine();
            while (firstName.length() > 45) {
                System.out.println("45 characters or less | Please enter a valid First Name:");
                firstName = entry.nextLine();
            }
            System.out.println("\n" + lastName + " | " + firstName +" |\n");

            System.out.println("Middle Initial:");
            String middleInitial = entry.nextLine();
            while (middleInitial.length() > 1) {
                System.out.println("1 characters or less | Please enter a valid Middle Initial:");
                middleInitial = entry.nextLine();
            }
            System.out.println("\n" + lastName + " | " + firstName + " | " + middleInitial + " |\n");

            System.out.println("Preferred Name:");
            String preferredName = entry.nextLine();
            while (preferredName.length() > 45) {
                System.out.println("45 characters or less | Please enter a valid Preferred Name:");
                preferredName = entry.nextLine();
            }
            System.out.println("\n" + lastName + " | " + firstName + " | " + middleInitial + " | " + preferredName + " |\n");

            /*
             *
             * Address Table:
             *
             * Zip to City & State: Start
             *
             */
            System.out.println("Street:");
            String street = entry.nextLine();
            while (street.length() > 45) {
                System.out.println("45 characters or less | Please enter a valid Street Name or Number:");
                street = entry.nextLine();
            }
            System.out.println("\n" + lastName + " | " + firstName + " | " + middleInitial + " | " + preferredName +
                    " | " + street + " |\n");

            System.out.println("Postal Code (Zip):");
            String postalCode = entry.nextLine();
            while (postalCode.length() > 5) {
                System.out.println("5 digit postal code required | Please re-enter the postal code:");
                postalCode = entry.nextLine();
            }

            /*
             * Secret keys usually stored in environment variables.
             * Authentication for API requests: Using smartystreets.com
             */
            String authId = "1c573638-e827-321f-bd5f-34785335d15f";
            String authToken = "F4KM9U3KZJS76LRov8ST";
            StaticCredentials credentials = new StaticCredentials(authId, authToken);

            Client client = new ClientBuilder(credentials).buildUsZipCodeApiClient();
            Lookup lookup = new Lookup();
            lookup.setZipCode(postalCode);

            try {
                client.send(lookup);
            }
            catch (SmartyException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            Result zipResult = lookup.getResult();
            City cities = zipResult.getCity(0);

            String city = cities.getCity();
            String state = cities.getState();

            System.out.println("City: " + city);
            System.out.println("State: " + state);
            /*
             * Zip to City & State: End
             *
             *
             * Static entry for country
             * Could be set like postal code with further API use
             */
            String country = "United States";
            System.out.println("Country: United States");
            System.out.println("\n" + lastName + " | " + firstName + " | " + middleInitial + " | " + preferredName +
                    " | " + street + " | " + postalCode + " | " + city + " | " + state + " | " + country + " |\n");

            /*
             *
             * Employee Table:
             *
             */
            System.out.println("Date of Birth (ex. 01-01-2020):");
            String dob = entry.nextLine();
            while (!Validate.checkDate(dob)) {
                System.out.println("Please enter date in the correct format (ex. 01-01-2020):");
                dob = entry.nextLine();
            }

            System.out.println("Social Security Number:");
            String ssn = entry.nextLine();
            while (!Validate.checkSSN(ssn)) {
                System.out.println("SSN entered is not valid | Please enter a valid SSN:");
                ssn = entry.nextLine();
            }

            System.out.println("Phone Number:");
            String phone = entry.nextLine();
            while (!Validate.checkPhone(phone)) {
                System.out.println("Valid phone number required | Please enter a valid phone number:");
                phone = entry.nextLine();
            }

            System.out.println("Email Address:");
            String email = entry.nextLine();
            while (!Validate.checkEmail(email)) {
                System.out.println("Email address required | Please enter a valid email address:");
                email = entry.nextLine();
            }

            System.out.println("Job Title:");
            String jobTitle = entry.nextLine();
            while (jobTitle.length() > 45) {
                System.out.println("45 characters or less | Please enter a valid Job Title:");
                jobTitle = entry.nextLine();
            }

            System.out.println("Department:");
            String department = entry.nextLine();
            while (department.length() > 45) {
                System.out.println("45 characters or less | Please enter a valid Department Name:");
                department = entry.nextLine();
            }

            System.out.println("Office Phone:");
            String officePhone = entry.nextLine();
            while (!Validate.checkPhone(officePhone)) {
                System.out.println("Valid phone number required | Please enter a valid phone number:");
                officePhone = entry.nextLine();
            }

            System.out.println("Office Room Number:");
            String officeNumber = entry.nextLine();
            while (officeNumber.length() > 15) {
                System.out.println("15 characters or less | Please enter a valid Room Number:");
                officeNumber = entry.nextLine();
            }

            System.out.println("Hire Date:");
            String hireDate = entry.nextLine();
            while (!Validate.checkDate(hireDate)) {
                System.out.println("Please enter date in the correct format (ex. 01-01-2020):");
                hireDate = entry.nextLine();
            }

            System.out.println("Training Completion Date:");
            String trainingCompleteDate = entry.nextLine();
            while (!Validate.checkDate(trainingCompleteDate)) {
                System.out.println("Please enter date in the correct format (ex. 01-01-2020):");
                trainingCompleteDate = entry.nextLine();
            }


            //create or initialize ManagerFactory using persistence.xml unit name
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
            EntityManager entityManager = entityManagerFactory.createEntityManager();

            int newNameID = 0;
            int newAddressID = 0;

            List<Object[]> resultList = entityManager.createNativeQuery(
                    "SELECT n.Name_ID, p.Address_ID\n" +
                            "FROM employee_db.employee e\n" +
                            "join employee_db.name n on (n.Name_ID)\n" +
                            "join employee_db.physical_address p on (p.Address_ID)\n" +
                            "ORDER BY n.Name_ID DESC, p.Address_ID DESC\n" +
                            "LIMIT 1;")
                    .getResultList();

            for (Object[] result : resultList) {
                int NameID = (int) result[0];
                int AddressID = (int) result[1];

                newNameID = NameID + 1;
                newAddressID = AddressID + 1;
            }
            //System.out.println(newNameID + " " + newAddressID);

            if (newNameID > 0 || newAddressID > 0) {
                Name name = new Name(lastName, firstName, middleInitial, preferredName);
                Physical_Address physicalAddress = new Physical_Address(street, city, state, postalCode, country);
                Employee employee = new Employee(newNameID, dob, ssn, phone, newAddressID, email, department, jobTitle, officePhone, officeNumber, hireDate, trainingCompleteDate);

                entityManager.getTransaction().begin();
                entityManager.persist(name);
                entityManager.getTransaction().commit();

                entityManager.getTransaction().begin();
                entityManager.persist(physicalAddress);
                entityManager.getTransaction().commit();

                entityManager.getTransaction().begin();
                entityManager.persist(employee);
                entityManager.getTransaction().commit();
            }
            else {
                System.out.println("Id's not greater than 0: " + newNameID + " | " + newAddressID);
            }

            entityManagerFactory.close();

            System.out.println("\n");
            Main.main(null);
        }
        catch (Exception e) {
            System.out.println("The system has failed to add an employee.");
        }
    }
}