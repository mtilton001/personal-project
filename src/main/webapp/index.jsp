<%--
  Created by IntelliJ IDEA.
  User: Matthew
  Date: 12/03/2020
  Time: 2:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}
            * { box-sizing: border-box; }

            input[type=text], select, textarea {
                width: 100%;
                padding: 12px;
                border: 2px solid #ADB9D3;
                border-radius: 4px;
                box-sizing: border-box;
                margin-top: 6px;
                margin-bottom: 16px;
                resize: vertical;
            }

            input[type=date], select, textarea {
                width: 100%;
                padding: 12px;
                border: 2px solid #ADB9D3;
                border-radius: 4px;
                box-sizing: border-box;
                margin-top: 6px;
                margin-bottom: 16px;
                resize: vertical;
            }

            input[type=submit] {
                background-color: #3A5795;
                color: white;
                padding: 18px 38px;
                border: none;
                border-radius: 18px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #637BAD;
            }

            .container {
                border-radius: 12px;
                background-color: #EDF0F5;
                padding: 18px;
            }
        </style>
        <title>Add Employee</title>
    </head>
    <body>
        <div class="container">
        <form action="Servlet" method="post">
            <h2>Employee Entry Form</h2>

            <label>Last Name:
                <input type="text" name="lastName">
            </label><br>

            <label>First Name:
                <input type="text" name="firstName">
            </label><br>

            <label>Middle Initial:
                <input type="text" name="middleInitial">
            </label><br>

            <label>Preferred Name:
                <input type="text" name="preferredName">
            </label><br>

            <label>Date of Birth:
                <input type="date" name="dob">
            </label><br>

            <label>SSN (ex. 000-00-0000):
                <input type="text" name="ssn">
            </label><br>

            <label>Street Address:
                <input type="text" name="street">
            </label><br>

            <label>Postal Code:
                <input type="text" name="postalCode">
            </label><br>

            <label>Phone Number:
                <input type="text" name="phoneNumber">
            </label><br>

            <label>Email:
                <input type="text" name="email">
            </label><br>

            <label>Department:
                <input type="text" name="department">
            </label><br>

            <label>Job Title:
                <input type="text" name="jobTitle">
            </label><br>

            <label>Office Phone:
                <input type="text" name="officePhone">
            </label><br>

            <label>Office Room Number:
                <input type="text" name="officeNumber">
            </label><br>

            <label>Hire Date:
                <input type="date" name="hireDate">
            </label><br>

            <label>Training Completion Date:
                <input type="date" name="trainingCompleteDate">
            </label><br>

            <input type="submit" value="Submit">
        </form>
        </div>
    </body>
</html>